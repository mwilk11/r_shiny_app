
library(shiny)
library(shinydashboard)
library(tidyverse)
library(data.table)
library(plotly)

econ2 <- fread(file = "./Trade_data_2.csv")
econ2$Year <- as.character(econ2$Year)

econ3 <- fread(file = "./Trade_category_data.csv")
econ3$Year <- as.character(econ3$Year)

econ4 <- fread(file = "./Trade_ATP_clean.csv")
econ4$Year <- as.character(econ4$Year)

econ5 <- fread(file = "./Trade_ATP_category.csv")
econ5$Year <- as.character(econ5$Year)

###################################################
######### ANALYSIS BELOW ##########################
###################################################

#total trade balance for 41 Countries and projected 
proj_tot_bal_2019 = sum(econ2[Year==2019,Balance])+(sum(econ2[Year==2019,Balance])/8)*4
tot_bal_2019 = sum(econ2[Year==2019,Balance])
tot_bal_2018 = sum(econ2[Year==2018,Balance])
tot_bal_2017 = sum(econ2[Year==2017,Balance])
tot_bal_aug_2018 = sum(econ2[(Year==2018 & Month<9),Balance])
tot_bal_aug_2018/8
tot_bal_aug_2017 = sum(econ2[(Year==2017 & Month<9),Balance])
tot_bal_aug_2017/8

#total imports for 41 Countries and projected 
tot_imp_2019 = sum(econ2[Year==2019,Imports])
proj_tot_imp_2019 = sum(econ2[Year==2019, Imports])+(sum(econ2[Year==2019,Imports])/8)*4
tot_imp_2018 = sum(econ2[Year==2018,Imports])
tot_imp_2017 = sum(econ2[Year==2017,Imports])
tot_imp_aug_2017 = sum(econ2[(Year==2017 & Month<9),Imports])
tot_imp_aug_2017/8
tot_imp_aug_2018 = sum(econ2[(Year==2018 & Month<9),Imports])
tot_imp_aug_2018/8

#total exports for 41 Countries and projected 
proj_tot_exp_2019 = sum(econ2[Year==2019,Exports])+(sum(econ2[Year==2019,Exports])/8)*4
tot_exp_2019 = sum(econ2[Year==2019,Exports])
tot_exp_2018 = sum(econ2[Year==2018,Exports])
tot_exp_2017 = sum(econ2[Year==2017,Exports])
tot_exp_aug_2017 = sum(econ2[(Year==2017 & Month<9),Exports])
tot_exp_aug_2018 = sum(econ2[(Year==2018 & Month<9),Exports])

#average per month figures for 41 Countries and projected 
avg_bal_per_mon_2019 = sum(econ2[Year==2019,Balance])/8
avg_bal_per_mon_2018 = sum(econ2[Year==2018,Balance])/12
avg_bal_per_mon_2017 = sum(econ2[Year==2017,Balance])/12

avg_imp_per_mon_2019 = sum(econ2[Year==2019,Imports])/8
avg_imp_per_mon_2018 = sum(econ2[Year==2018,Imports])/12
avg_imp_per_mon_2017 = sum(econ2[Year==2017,Imports])/12

avg_exp_per_mon_2019 = sum(econ2[Year==2019,Exports])/8
avg_exp_per_mon_2018 = sum(econ2[Year==2018,Exports])/12
avg_exp_per_mon_2017 = sum(econ2[Year==2017,Exports])/12

length(unique(econ2$Country))

econ2 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Balance, Country) %>%
  group_by(Country) %>%
  summarise(sum_bal_2019 = sum(Balance)) -> bal_2019_2

econ2 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Balance, Country) %>%
  group_by(Country) %>%
  summarise(prev_year = sum(Balance)) -> bal_2019_2$prev_year

bal_2019_2$diff = bal_2019_2[,2] - bal_2019_2$prev_year[,2]

econ2 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Imports, Country) %>%
  group_by(Country) %>%
  summarise(sum_IMP_2019 = sum(Imports)) -> IMP_18to19

econ2 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Imports, Country) %>%
  group_by(Country) %>%
  summarise(prev_year = sum(Imports)) -> IMP_18to19$prev_year

IMP_18to19$diff = IMP_18to19[,2] - IMP_18to19$prev_year[,2]

econ2 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Exports, Country) %>%
  group_by(Country) %>%
  summarise(sum_EXP_2019 = sum(Exports)) -> EXP_18to19

econ2 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Exports, Country) %>%
  group_by(Country) %>%
  summarise(prev_year = sum(Exports)) -> EXP_18to19$prev_year

EXP_18to19$diff = EXP_18to19[,2] - EXP_18to19$prev_year[,2]

#COMMODITY CATEGORIES

#EXPORTS 

econ3 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Exports, Category) %>%
  group_by(Category) %>%
  summarise(sum_EXP_2019 = sum(Exports)) -> EXP_CAT_18to19

econ3 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Exports, Category) %>%
  group_by(Category) %>%
  summarise(prev_year = sum(Exports)) -> EXP_CAT_18to19$prev_year

EXP_CAT_18to19$diff = EXP_CAT_18to19[,2] - EXP_CAT_18to19$prev_year[,2]

#IMPORTS 

econ3 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Imports, Category) %>%
  group_by(Category) %>%
  summarise(sum_IMP_2019 = sum(Imports)) -> IMP_CAT_18to19

econ3 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Imports, Category) %>%
  group_by(Category) %>%
  summarise(prev_year = sum(Imports)) -> IMP_CAT_18to19$prev_year

IMP_CAT_18to19$diff = IMP_CAT_18to19[,2] - IMP_CAT_18to19$prev_year[,2]

#ATP COUNTRIES 

#BALANCE

econ4 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Balance, Country) %>%
  group_by(Country) %>%
  summarise(sum_BAL_2019 = sum(Balance)) -> BAL_ATP_18to19

econ4 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Balance, Country) %>%
  group_by(Country) %>%
  summarise(prev_year = sum(Balance)) -> BAL_ATP_18to19$prev_year

BAL_ATP_18to19$diff = BAL_ATP_18to19[,2] - BAL_ATP_18to19$prev_year[,2]

# IMPORTS

econ4 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Imports, Country) %>%
  group_by(Country) %>%
  summarise(sum_IMP_2019 = sum(Imports)) -> IMP_ATP_18to19

econ4 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Imports, Country) %>%
  group_by(Country) %>%
  summarise(prev_year = sum(Imports)) -> IMP_ATP_18to19$prev_year

IMP_ATP_18to19$diff = IMP_ATP_18to19[,2] - IMP_ATP_18to19$prev_year[,2]

#EXPORTS

econ4 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Exports, Country) %>%
  group_by(Country) %>%
  summarise(sum_EXP_2019 = sum(Exports)) -> EXP_ATP_18to19

econ4 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Exports, Country) %>%
  group_by(Country) %>%
  summarise(prev_year = sum(Exports)) -> EXP_ATP_18to19$prev_year

EXP_ATP_18to19$diff = EXP_ATP_18to19[,2] - EXP_ATP_18to19$prev_year[,2]

#ATP PRODUCT CATEGORIES

#BALANCE

econ5 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Balance, Category) %>%
  group_by(Category) %>%
  summarise(sum_BAL_2019 = sum(Balance)) -> BAL_ATP_CAT_18to19

econ5 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Balance, Category) %>%
  group_by(Category) %>%
  summarise(prev_year = sum(Balance)) -> BAL_ATP_CAT_18to19$prev_year

BAL_ATP_CAT_18to19$diff = BAL_ATP_CAT_18to19[,2] - BAL_ATP_CAT_18to19$prev_year[,2]

# IMPORTS

econ5 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Imports, Category) %>%
  group_by(Category) %>%
  summarise(sum_IMP_2019 = sum(Imports)) -> IMP_ATP_CAT_18to19

econ5 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Imports, Category) %>%
  group_by(Category) %>%
  summarise(prev_year = sum(Imports)) -> IMP_ATP_CAT_18to19$prev_year

IMP_ATP_CAT_18to19$diff = IMP_ATP_CAT_18to19[,2] - IMP_ATP_CAT_18to19$prev_year[,2]

#EXPORTS

econ5 %>%
  filter(Year==2019 & Month<9) %>%
  select(Year, Month, Exports, Category) %>%
  group_by(Category) %>%
  summarise(sum_EXP_2019 = sum(Exports)) -> EXP_ATP_CAT_18to19

econ5 %>%
  filter(Year==2018 & Month<9) %>%
  select(Year, Month, Exports, Category) %>%
  group_by(Category) %>%
  summarise(prev_year = sum(Exports)) -> EXP_ATP_CAT_18to19$prev_year

EXP_ATP_CAT_18to19$diff = EXP_ATP_CAT_18to19[,2] - EXP_ATP_CAT_18to19$prev_year[,2]


