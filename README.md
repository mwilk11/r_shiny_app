# R_shiny_app

This interactive app uses data from the US Census Bureau to be able to view and compare trade balance, imports and exports between the United States and 41 other countries as well as commodity categories and advanced technology trade since 2017. This app can be accessed at: https://mwilk.shinyapps.io/ustradedata/

*App is now updated to include data through November 2019
